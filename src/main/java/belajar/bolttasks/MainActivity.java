package belajar.bolttasks;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;

public class MainActivity extends AppCompatActivity {


    private Button tombols;
    private Button tombolsbatal;
    private TextView tekshasil;
    private String strhasils = "";

    private ProgressDialog dialogprogress;
    private Task<String> taskstring;


    CancellationTokenSource cancellationTokenSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tombols = (Button) findViewById(R.id.tombol_bolt);
        tombols.setOnClickListener(listener);

        tombolsbatal = (Button) findViewById(R.id.tombol_batalbolt);
        tombolsbatal.setOnClickListener(listenerbatal);

        tekshasil = (TextView) findViewById(R.id.tekshasil);


        dialogprogress = new ProgressDialog(MainActivity.this);
        dialogprogress.setMessage("TES BOLTS TASK");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        cancellationTokenSource.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void jalanKanTaskBoltsSatu() {

        dialogprogress.show();

        //jalankan di background
        Task.callInBackground(new Callable<String>() {

            @Override
            public String call() throws Exception {

                //sampel proses di background
                Log.w("JALAN", "TASK JALAN DI BACKGROUND");
                String jalantask = "HASIL : OK JALAN";

                //contoh proses di balik layar
                for (int i = 0; i < 100; i++) {

                    Log.w("PUTAR LOOPS", "TASK JALAN DI BACKGROUND " + i);
                    Thread.sleep(200);
                }


                return jalantask;
            }
        })

                //jika selesai proses di background, lanjut tampilkan ke thread utama
                .onSuccess(new Continuation<String, Object>() {
                    @Override
                    public Object then(Task<String> task) throws Exception {

                        Log.w("PUTAR SELESAI", "TASK SELESAI");

                        shows(task.getResult());
                        dialogprogress.dismiss();


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR);
    }


    private void jalanKanTaskBoltsCancel() {

        dialogprogress.show();
        cancellationTokenSource = new CancellationTokenSource();

        Task.callInBackground(new Callable<String>() {

            @Override
            public String call() throws Exception {

                Log.w("JALAN", "TASK JALAN DI BACKGROUND");
                String jalantask = "HASIL : OK JALAN";

                //contoh proses di balik layar
                for (int i = 0; i < 200; i++) {

                    if (cancellationTokenSource.getToken().isCancellationRequested()) {

                        break;

                    } else {

                        Log.w("PUTAR LOOPS", "TASK JALAN DI BACKGROUND " + i);
                        Thread.sleep(200);
                    }
                }

                return jalantask;
            }
        }, cancellationTokenSource.getToken())


                .onSuccess(new Continuation<String, Object>() {
                    @Override
                    public Object then(Task<String> task) throws Exception {

                        Log.w("PUTAR SELESAI", "TASK SELESAI");

                        shows(task.getResult());
                        dialogprogress.dismiss();


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, cancellationTokenSource.getToken());
    }


    private void jalanKanTaskBoltsCancel2() {

        cancellationTokenSource = new CancellationTokenSource();
        tekshasil.setText("TASK JALAN");

        Task.callInBackground(new Callable<String>() {

            @Override
            public String call() throws Exception {

                Log.w("JALAN", "TASK JALAN DI BACKGROUND");
                String jalantask = "HASIL : OK JALAN";

                //contoh proses di balik layar
                for (int i = 0; i < 200; i++) {

                    Log.w("PUTAR LOOPS", "TASK JALAN DI BACKGROUND " + i);
                    Thread.sleep(200);

                }

                return jalantask;
            }
        }, cancellationTokenSource.getToken())

                //TIDAK JALAN JIKA STATUS TOKEN CANCELATION NYA SUDAH BATAL
                .onSuccess(new Continuation<String, Object>() {
                    @Override
                    public Object then(Task<String> task) throws Exception {

                        if (cancellationTokenSource.getToken().isCancellationRequested()) {

                            tekshasil.setText("BATAL");

                        }

                        Log.w("PUTAR SELESAI", "TASK SELESAI");

                        shows(task.getResult());
                        dialogprogress.dismiss();


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, cancellationTokenSource.getToken());
    }


    public void shows(String strs) {

        Log.w("JALAN", "TASK JALAN DI SELESAI");

        Toast.makeText(MainActivity.this, "SUKSES " + strs, Toast.LENGTH_SHORT).show();
        tekshasil.setText(strs);
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            jalanKanTaskBoltsSatu();
//            jalanKanTaskBoltsCancel();
//            jalanKanTaskBoltsCancel2();
        }
    };


    View.OnClickListener listenerbatal = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            cancellationTokenSource.cancel();

        }
    };


}
